from setuptools import setup

setup(
    name='Slash',
    version='0.1',
    description='Slurm And Smaller Headaches',
    author='Maximilian Huebl',
    author_email='maximilian.huebl@ist.ac.at',
    install_requires=['dill']
)
