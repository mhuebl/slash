# <ins>SL</ins>urm <ins>A</ins>nd <ins>S</ins>maller <ins>H</ins>eadaches (<ins>SLASH</ins>)

This is a very low tech convenience wrapper for submitting SLURM jobs from within Python.
It allows you to connect to a remote computing cluster that runs SLURM and submit jobs directly from your local machine.
[TODO: MORE EXPLANATORY TEXT]
The only dependency is `dill`, which is used for serialization.

Check out the tutorial notebook for a quickstart, or look through the code to see what else is possible.