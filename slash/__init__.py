from .slash import SlashTask, SlashArrayTask, submitted, deployed, smapped, \
    settings, poll_archived_tasks, load_archived_task, clear_archived_tasks
slash_settings = settings

__all__ = ['SlashTask', 'SlashArrayTask',
           'slash_settings',
           'submitted', 'deployed', 'smapped',
           'poll_archived_tasks', 'load_archived_task', 'clear_archived_tasks']
