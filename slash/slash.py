import dill as pickle
import uuid

import pathlib
import os

import subprocess as sp
import shlex

from copy import copy
from dataclasses import dataclass
import functools as ft


@dataclass
class SlashSettings:
    local_rootdir: str
    host_rootdir: str
    hostname: str
    username: str
    host_conda_env: str
    host_imports: list
    local_imports: list
    slurm_settings: dict


settings = SlashSettings(local_rootdir='/tmp', host_rootdir='', hostname='', username='', host_conda_env='base',
                         host_imports=[''], local_imports=[''],
                         slurm_settings={'nodes': 1, 'cpus-per-task': 1, 'mem': '1G', 'time': '00:01:00'})


class SlashTask:
    def __init__(self, f, args, kwargs):
        self.task = f
        self.args = (args, kwargs)
        self.task_name = str(uuid.uuid4())

        self.status = 'idle'
        self.job_id = None
        self.execution_process = None

        local_rootdir = pathlib.Path(settings.local_rootdir)
        host_rootdir = pathlib.Path(settings.host_rootdir)

        self.task_file_local = local_rootdir / (self.task_name + '_task.pkl')
        self.payload_file_local = local_rootdir / (self.task_name + '_payload.pkl')
        self.result_file_local = local_rootdir / (self.task_name + '_result.pkl')

        self.payload_file_remote = host_rootdir / (self.task_name + '_payload.pkl')
        self.script_file_remote = host_rootdir / (self.task_name + '_exec.py')
        self.result_file_remote = host_rootdir / (self.task_name + '_result.pkl')
        self.log_file_remote = host_rootdir / (self.task_name + '_log.txt')

        self.host_imports = copy(settings.host_imports)
        # TODO: maybe copy all active settings

        self.result = None
        self.log_msg = None

    # TODO: think about what to clean up if garbage collected
    def __del__(self):
        self._remove_local_files(delete_archive=False)
        return

    # TODO: move this to __str__ and include full argument list in __repr__
    def __repr__(self):
        function_name = self.task.__name__
        job_id = self.job_id if self.job_id is not None else '-'
        desc = f'Task {self.task_name}: Evaluate {function_name}{self.args}\n' + \
               f'Status: {self.status} \t Job ID: {job_id}'
        return desc

    def cleanup(self, delete_archive=False, silent=True):
        exit_code = self._remove_remote_files()
        if exit_code != 0 and not silent:
            print(f'warning: could not cleanup remote files (they may already be deleted). exit code {exit_code}')

        self._remove_local_files(delete_archive)
        return

    def transfer(self):
        self._serialize()   # Create serialized payload file

        payload_transfer_cmd = shlex.split(
            f"scp {self.payload_file_local} {settings.username}@{settings.hostname}:{self.payload_file_remote}")

        script_transfer_cmd = self._generate_script_transfer_command()
        ssh_connect_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{script_transfer_cmd}\"")

        payload_transfer_process = sp.Popen(payload_transfer_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        script_transfer_process = sp.Popen(ssh_connect_cmd, stdout=sp.PIPE, stderr=sp.PIPE)

        exit_code_payload = payload_transfer_process.wait()
        exit_code_script = script_transfer_process.wait()

        os.remove(self.payload_file_local)   # Remove serialized payload file

        if exit_code_payload == 0 and exit_code_script == 0:
            self.status = 'transferred'
            return 0
        else:
            raise RuntimeError(
                f'file transfer failed with exit codes {exit_code_payload} '
                f'(payload) and {exit_code_script} (execution script)')

    def retrieve_result(self, timeout=30, show_log=False):
        if self.result is not None:
            if show_log:
                print(self._retrieve_log_message())
            return self.result

        running = self.poll(return_running=True, silent=True)
        if running:
            print(f"unable to retrieve result: job {self.job_id} is still running")
            return None

        return self._fetch_result(timeout, show_log)

    def run(self):
        if self.status == 'idle':
            raise RuntimeError('task has not been transferred to remote')
        elif self.status == 'running' or self.status == 'finished':
            return self.execution_process

        self.execution_process = None
        self.job_id = None

        execution_process, _ = self._remote_execute_task(submit=False)
        self.execution_process = execution_process

        self.status = 'running'
        return execution_process

    def submit(self):
        if self.status == 'idle':
            raise RuntimeError('task has not been transferred to remote')
        elif self.status == 'running' or self.status == 'finished':
            return self.job_id

        self.execution_process = None
        self.job_id = None

        execution_process, job_id = self._remote_execute_task(submit=True)

        submit_exit_code = execution_process.wait()
        if submit_exit_code == 0:
            self.job_id = job_id
            self.status = 'running'
            return job_id
        else:
            raise RuntimeError(f'job submission failed with exit code {submit_exit_code}')

    def poll(self, return_running=False, silent=False):
        if self.status == 'running':
            poll_cmd = f'squeue | grep {self.job_id}'
            ssh_poll_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{poll_cmd}\"")
            poll_process = sp.Popen(ssh_poll_cmd, stdout=sp.PIPE, stderr=sp.PIPE)

            output, _ = poll_process.communicate()
            output = output.decode('UTF-8')

            running = str(self.job_id) in output.split()
            self.status = 'running' if running else 'finished'

            if not silent:
                print(output)
        elif self.status == 'finished':
            if not silent:
                print("task has finished")
            running = False
        else:
            if not silent:
                print("task is currently not submitted")
            running = False

        if return_running:
            return running
        else:
            return

    def cancel(self, timeout=20, silent=False):
        if self.status == 'running':
            cancel_cmd = f'scancel {self.job_id}'
            ssh_cancel_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{cancel_cmd}\"")
            cancel_process = sp.Popen(ssh_cancel_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
            exit_code = cancel_process.wait(timeout=timeout)
            if exit_code == 0:
                self.status = 'transferred'
                self.job_id = None
                self.execution_process = None
                if not silent:
                    print(f'job {self.job_id} has been cancelled')
            else:
                print(f'warning: job {self.job_id} could not be cancelled (exit code {exit_code})')
        else:
            pass
        return

    def archive(self):
        _pickle_file(self, self.task_file_local)
        return

    def _serialize(self):
        args, kwargs = self.args
        worker = ft.partial(self.task, *args, **kwargs)

        payload = (worker, self.result_file_remote.name)
        _pickle_file(payload, self.payload_file_local)
        return

    def _fetch_result(self, timeout, show_log):
        retrieve_result_cmd = shlex.split(
            f"scp {settings.username}@{settings.hostname}:{self.result_file_remote} {self.result_file_local}")
        retrieve_process = sp.Popen(retrieve_result_cmd, stdout=sp.PIPE, stderr=sp.PIPE)

        if show_log:
            print(self._retrieve_log_message())

        exit_code = retrieve_process.wait(timeout)
        if exit_code == 0:
            result = _unpickle_file(self.result_file_local)
            self.result = result
            self.status = 'finished'
            os.remove(self.result_file_local)
            return result
        else:
            raise RuntimeError(f"retrieving results failed with exit code {exit_code}")

    def _remote_execute_task(self, submit):
        run_cmd = self._generate_run_command(submit)
        ssh_execution_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{run_cmd}\"")

        execution_process = sp.Popen(ssh_execution_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        if submit:
            output, err = execution_process.communicate()
            if len(err) > 0:
                print(err)
            output = output.decode('UTF-8')
            output = output.split()
            job_id_idx = output.index('job') + 1
            try:
                job_id = int(output[job_id_idx])
            except ValueError:
                job_id = None
                print('warning: unable to fetch job ID, please use `squeue` to find job ID manually')
        else:
            job_id = None  # TODO: could still read the job id from a directly run process
        return execution_process, job_id

    def _generate_run_command(self, submit):
        env_cmd = f"conda activate {settings.host_conda_env}"

        submitter = 'sbatch' if submit else 'srun'
        task_cmd = '--wrap ' + '\'python ' + self.script_file_remote.name + '\'' if submit \
            else 'python ' + self.script_file_remote.name

        run_cmd = f"{submitter} -D {settings.host_rootdir} -J \"{self.task.__name__}\" " \
                  f"-o \"{self.log_file_remote.name}\" " + ''.join(
                    [f'--{k} {v} ' for k, v in settings.slurm_settings.items()]) + task_cmd

        run_cmd = env_cmd + '; ' + run_cmd
        return run_cmd

    def _generate_script_transfer_command(self):
        imports = "".join(i + '\n' for i in self.host_imports)

        # Everything needs to be escaped, as the shell is going to interpret this script
        script = (f'import dill as pickle\n'
                  f'import os\n{imports}\n'
                  f'with open(\\"{self.payload_file_remote.name}\\", \\"rb\\") as file:\n'
                  '\tpayload = pickle.load(file)\n'
                  'worker, output_filename = payload\n'
                  'result = worker()\n'
                  'with open(output_filename, \\"wb\\") as file:\n'
                  '\tpickle.dump(result, file)\n'
                  f'os.remove(\\"{self.payload_file_remote.name}\\")\n'
                  f'os.remove(\\"{self.script_file_remote.name}\\")\n')

        script_transfer_cmd = f"echo '{script}' > {self.script_file_remote}"
        return script_transfer_cmd

    def _retrieve_log_message(self):
        if self.log_msg is not None:
            return self.log_msg

        show_log_cmd = f'cat {self.log_file_remote}'
        ssh_show_log_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{show_log_cmd}\"")
        show_log_process = sp.Popen(ssh_show_log_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        output, _ = show_log_process.communicate()
        output = output.decode('UTF-8')
        self.log_msg = output
        return self.log_msg

    def _remove_remote_files(self):
        remote_remove_cmd = f"rm {self.payload_file_remote} {self.script_file_remote} " \
                            f"{self.result_file_remote} {self.log_file_remote}"
        ssh_connect_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{remote_remove_cmd}\"")
        ssh_process = sp.Popen(ssh_connect_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        exit_code = ssh_process.wait(timeout=20)
        return exit_code

    def _remove_local_files(self, delete_archive):
        _silent_remove(self.payload_file_local)
        _silent_remove(self.result_file_local)
        if delete_archive:
            _silent_remove(self.task_file_local)
        return


class SlashArrayTask(SlashTask):
    def __init__(self, f, args, kwargs=None):
        args, kwargs, n_args = self._align_args(args, kwargs)
        self.n_args = n_args
        super(SlashArrayTask, self).__init__(f, args, kwargs)

        host_rootdir = pathlib.Path(settings.host_rootdir)
        self.shell_file_remote = host_rootdir / (self.task_name + '_run.sh')

        self.result_file_remote = [self.result_file_remote.with_stem(self.result_file_remote.stem + f'_{i}')
                                   for i in range(self.n_args)]
        self.result_file_local = [self.result_file_local.with_stem(self.result_file_local.stem + f'_{i}')
                                   for i in range(self.n_args)]

    def __repr__(self):
        function_name = self.task.__name__
        job_id = self.job_id if self.job_id is not None else '-'
        desc = f'Array Task {self.task_name}: Evaluate {function_name} on {self.n_args} arguments\n' + \
               f'Status: {self.status} \t Job ID: {job_id}'
        return desc

    @staticmethod
    def _align_args(args, kwargs):
        n_args = len(args)
        if kwargs is None:
            kwargs = [{} for _ in range(n_args)]

        assert n_args == len(kwargs)
        return args, kwargs, n_args

    def _fetch_result(self, timeout, show_log):
        host_rootdir = self.result_file_remote[0].parent
        local_rootdir = self.result_file_local[0].parent

        result_files_remote_list = ','.join([r.name for r in self.result_file_remote])
        retrieve_result_cmd = shlex.split(
            f"scp {settings.username}@{settings.hostname}:{host_rootdir}/\\{{{result_files_remote_list}\\}} "
            f"{local_rootdir}")
        retrieve_process = sp.Popen(retrieve_result_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        if show_log:
            print(self._retrieve_log_message())

        exit_code = retrieve_process.wait(timeout)
        if exit_code == 0:
            result = [_unpickle_file(f) for f in self.result_file_local]
            self.result = result
            self.status = 'finished'
            for f in self.result_file_local:
                os.remove(f)
            return result
        else:
            raise RuntimeError(f"retrieving results failed with exit code {exit_code}")

    def _generate_run_command(self, submit):
        task_count = self.n_args

        env_cmd = f"conda activate {settings.host_conda_env}"

        submitter = 'sbatch' if submit else 'srun'

        run_cmd = f"{submitter} --array 0-{task_count-1} -D {settings.host_rootdir} -J \"{self.task.__name__}\" " \
                  f"-o \"{self.log_file_remote.name}\" " + ''.join(
                    [f'--{k} {v} ' for k, v in settings.slurm_settings.items()]) + self.shell_file_remote.name

        run_cmd = env_cmd + '; ' + run_cmd
        return run_cmd

    def _serialize(self):
        worker = (self.task, self.args[0], self.args[1])

        payload = (worker, self.task_name + '_result')
        _pickle_file(payload, self.payload_file_local)
        return

    def _generate_script_transfer_command(self):
        imports = "".join(i + '\n' for i in self.host_imports)

        # Everything needs to be escaped, as the shell is going to interpret this script
        pyscript = (f'import dill as pickle\n'
                    f'import sys\n'
                    f'import os\n{imports}\n'
                    f'task_idx = int(sys.argv[1])\n'
                    f'with open(\\"{self.payload_file_remote.name}\\", \\"rb\\") as file:\n'
                    '\tpayload = pickle.load(file)\n'
                    '(f, args, kwargs), output_filename = payload\n'
                    'result = f(*args[task_idx], **kwargs[task_idx])\n'
                    'with open(output_filename + \\"_\\" + str(task_idx) + \\".pkl\\", \\"wb\\") as file:\n'
                    '\tpickle.dump(result, file)\n')

        shscript = '#!/bin/bash\npython ' + self.script_file_remote.name + ' $SLURM_ARRAY_TASK_ID'

        script_transfer_cmd = f"echo '{pyscript}' > {self.script_file_remote}; " \
                              f"echo '{shscript}' > {self.shell_file_remote}"
        return script_transfer_cmd

    def _remove_remote_files(self):
        result_files = [f.as_posix() for f in self.result_file_remote]
        remote_remove_cmd = f"rm {self.payload_file_remote} {self.script_file_remote} " \
                            f"{self.log_file_remote} {self.shell_file_remote} {' '.join(result_files)}"
        ssh_connect_cmd = shlex.split(f"ssh {settings.username}@{settings.hostname} \"{remote_remove_cmd}\"")
        ssh_process = sp.Popen(ssh_connect_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
        exit_code = ssh_process.wait(timeout=20)
        return exit_code

    def _remove_local_files(self, delete_archive):
        _silent_remove(self.payload_file_local)
        for f in self.result_file_local:
            _silent_remove(f)
        if delete_archive:
            _silent_remove(self.task_file_local)
        return


def _unpickle_file(filename):
    with open(filename, 'rb') as file:
        data = pickle.load(file)
    return data


def _pickle_file(data, filename):
    with open(filename, 'wb') as file:
        pickle.dump(data, file)
    return


def _silent_remove(file):
    try:
        os.remove(file)
    except FileNotFoundError:
        pass
    return


def deployed(f):
    @ft.wraps(f)
    def wrapper(*args, **kwargs):
        st = SlashTask(f, args, kwargs)

        print("transferring...", end='\t\t\t')
        st.transfer()
        print('done')

        print("running remote task...", end='\t')
        remote_process = st.run()
        exit_code = remote_process.wait()
        if exit_code != 0:
            raise RuntimeError(f'remote execution failed with exit code {exit_code}')
        print('done')

        print("retrieving result...", end='\t')
        result = st.retrieve_result()
        print('done')
        st.cleanup()
        return result

    return wrapper


def submitted(f):
    @ft.wraps(f)
    def wrapper(*args, **kwargs):
        st = SlashTask(f, args, kwargs)

        print("transferring...", end='\t\t\t\t')
        st.transfer()
        print('done')

        print("submitting remote job...", end='\t')
        job_id = st.submit()
        print('done')
        print(f'successfully submitted job {job_id}')
        return st

    return wrapper


def smapped(f):
    @ft.wraps(f)
    def wrapper(arg_list, kwarg_list=None):
        sat = SlashArrayTask(f, arg_list, kwarg_list)

        print("transferring...", end='\t\t\t\t')
        sat.transfer()
        print('done')

        print("submitting remote jobs...", end='\t')
        job_id = sat.submit()
        print('done')
        print(f'successfully submitted array job {job_id}')
        return sat

    return wrapper


def poll_archived_tasks(directory=None):
    if directory is None:
        directory = pathlib.Path(settings.local_rootdir)

    task_files = [f for f in os.listdir(directory) if f.endswith('_task.pkl')]
    if len(task_files) == 0:
        return

    for i, task_file in enumerate(task_files):
        task = _unpickle_file(task_file)
        was_running = task.status == 'running'
        running = task.poll(return_running=True, silent=True)
        print('---------------------------------------------')
        print(f'{i}:', end=' ')
        print(task)
        if running != was_running:
            _pickle_file(task, task_file)
    print('---------------------------------------------')
    return


def load_archived_task(name_or_number, directory=None):
    if type(name_or_number) is str:
        name = name_or_number + '_task'
        idx = None
    elif type(name_or_number) is int:
        name = None
        idx = name_or_number
    else:
        raise ValueError('please provide a task name or number')

    if directory is None:
        directory = pathlib.Path(settings.local_rootdir)

    task_files = [f for f in os.listdir(directory) if f.endswith('_task.pkl')]

    task = None
    for i, task_file in enumerate(task_files):
        tn = pathlib.Path(task_file).stem
        if tn == name or i == idx:
            task = _unpickle_file(task_file)
            break

    if task is None:
        qualifier = 'no. ' if idx is not None else ''
        raise FileNotFoundError(f'task {qualifier}could not be found in {directory}')
    return task


def clear_archived_tasks(directory):
    task_files = [f for f in os.listdir(directory) if f.endswith('_task.pkl')]
    for task_file in task_files:
        task = _unpickle_file(task_file)
        task.cleanup(delete_archive=True)
    return

